package com.blueprint.galleryclient.activities;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.blueprint.galleryclient.BuildConfig;
import com.blueprint.galleryclient.R;
import com.blueprint.galleryclient.fragments.AccountFragment;
import com.blueprint.galleryclient.fragments.RemoteGalleryFragment;
import com.blueprint.galleryclient.presenters.MainActivityPresenter;
import com.blueprint.galleryclient.presenters.UserInfoPresenter;
import com.blueprint.galleryclient.data.repositories.FileSystemRepository;
import com.blueprint.galleryclient.views.MainActivityView;
import com.blueprint.galleryclient.views.UserInfoView;
import com.squareup.picasso.Picasso;

import java.io.File;

import pub.devrel.easypermissions.EasyPermissions;

import static com.blueprint.galleryclient.Const.*;

public class MainActivity extends AppBaseActivity implements UserInfoView, MainActivityView {

    private static final int RC_CAMERA_AND_STORAGE = 12;
    private static final String _TAG = "MainActivityTag";
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private TextView drawerUserName;
    private ImageView drawerUserAvatar;
    private UserInfoPresenter userInfoPresenter;
    private MainActivityPresenter mainActivityPresenter;
    private FrameLayout fragmentContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_cl);
        initViewFields();
        initNavigationView();
        Button b2 = findViewById(R.id.button_newPhoto);
        Button b3 = findViewById(R.id.button_fromSD);

        // Attaching the layout to the toolbar object
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Setting toolbar as the ActionBar with setSupportActionBar() call
        setSupportActionBar(toolbar);

        userInfoPresenter = new UserInfoPresenter(this, repository);
        mainActivityPresenter = new MainActivityPresenter(
                FileSystemRepository.getInstance(getBaseContext()),
                repository,
                this
        );

        b3.setOnClickListener(view -> {
            if (requestCameraPermissions())
                chooseImageFromGallery();
        });
        b2.setOnClickListener(v -> {
            if (requestCameraPermissions())
                mainActivityPresenter.takeImageFromCamera();
        });

        showSelectedFragment(R.id.nav_gallery);

    }

    private void initViewFields() {
        drawerLayout = findViewById(R.id.main_drawer);
        navigationView = findViewById(R.id.nav_view);
        drawerUserAvatar = navigationView.getHeaderView(0).findViewById(R.id.user_avatar);
        drawerUserName = navigationView.getHeaderView(0).findViewById(R.id.user_name);
        fragmentContainer = findViewById(R.id.fragment_container);

    }

    private void initNavigationView() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                showSelectedFragment(item.getItemId());
                return true;
            }
        });
    }

    private void showSelectedFragment(int itemId) {
        MenuItem item = navigationView.getMenu().findItem(itemId);
        if (item == null) return;
        item.setChecked(true);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = null;
        switch (itemId) {
            case R.id.nav_gallery:
                fragment = RemoteGalleryFragment.newInstance();
                break;
            case R.id.nav_account_settings:
                fragment = AccountFragment.newInstance();
                break;
        }
        drawerLayout.closeDrawers();
        if (fragment == null) return;
        FragmentTransaction t;
        t = fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment);
        if (!item.isChecked())
            t.addToBackStack(fragment.getClass().toString());
        t.commit();

    }

    private void refreshCurrentScreen() {
    }


    @Override
    protected void onConnectionEstablished() {
        super.onConnectionEstablished();
//        loadImages();
    }

    @Override
    protected void onConnectionLost() {
        super.onConnectionLost();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mainActivityPresenter.onAttach(this);
        userInfoPresenter.onAttach(this);
        mainActivityPresenter.checkUserAuthorization();
        userInfoPresenter.loadUserInfo();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mainActivityPresenter.onDettach();
        userInfoPresenter.onDettach();
    }

    boolean leftToRightSlide;

    @Override
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int y = (int) motionEvent.getY();
        int x;
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            x = (int) motionEvent.getX();
            if (x < getResources().getDimensionPixelSize(R.dimen.drawer_area_size)) {
                leftToRightSlide = true;
                drawerLayout.dispatchTouchEvent(motionEvent);
//                    return true;
            }
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_options, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_signOut:
                getSharedPreferences(getPackageName(), MODE_PRIVATE)
                        .edit()
                        .putString("Token", "")
                        .apply();

                repository.logOut();
                Intent i = new Intent(MainActivity.this, AuthActivity.class);
                startActivityForResult(i, AUTH_REQUEST);
                break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        File sourceFile = null;
        File image = null;

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            image = mainActivityPresenter.getImageTakenFromCamera();
        } else if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
                image = mainActivityPresenter.getImageTakenFromGallery(uri);
            } else {

            }

        }
        if (image != null) {
            Intent intent = new Intent(this, PictureOptionsActivity.class);
            intent.putExtra("photo", image);
            startActivity(intent);
        }

        if (requestCode == AUTH_REQUEST && resultCode == RESULT_OK) {
            userInfoPresenter.forceLoadUserData();
//            mainRepository.onLoggedIn(this, );
        }

        if (resultCode == QUIT) {
            finish();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    private boolean requestCameraPermissions() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            Log.d(_TAG, "requestCameraPermissions Already have permission, do the thing\n");
            // Already have permission, do the thing
            return true;
        } else {
            // Do not have permissions, request them now
            Log.d(_TAG, "requestCameraPermissions Do not have permissions, request them now\n");
            EasyPermissions.requestPermissions(this, "Чтобы сделать фото приложению нужен доступ к камере и памяти устройства",
                    RC_CAMERA_AND_STORAGE, perms);
            return false;
        }
    }

    private void chooseImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }


    @Override
    public void setUserName(String name) {
        drawerUserName.setText(name);
    }

    @Override
    public void setUserAvatar(String url) {
        Log.d(_TAG, "setUserAvatar: " + url);
        Picasso.with(this).load(url)
                .centerCrop().fit().into(drawerUserAvatar);
    }

    @Override
    public void showLoadingView() {

    }

    @Override
    public void hideLoadingView() {

    }

    @Override
    public void selectNavItem(int n) {

    }

    @Override
    public void startCamera(File photoFile) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            Uri photoURI = FileProvider.getUriForFile(this,
                    BuildConfig.APPLICATION_ID + ".fileprovider",
                    photoFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

        }
    }

    @Override
    public void showLoginScreen() {
        Intent i = new Intent(MainActivity.this, AuthActivity.class);
        startActivityForResult(i, AUTH_REQUEST);
    }
}
