package com.blueprint.galleryclient.activities;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import com.blueprint.galleryclient.ReceiverObservable;
import com.blueprint.galleryclient.data.LocalDataSource;
import com.blueprint.galleryclient.network.NetworkHelper;
import com.blueprint.galleryclient.data.RemoteDataSource;
import com.blueprint.galleryclient.data.repositories.MainRepository;
import com.blueprint.galleryclient.data.DataSource;
//import net.hockeyapp.android.CrashManager;
//import net.hockeyapp.android.UpdateManager;

import io.reactivex.disposables.Disposable;

/**
 * Created on 23.02.2018.
 */

public abstract class AppBaseActivity extends AppCompatActivity {
    private static final String _TAG = "AppBaseActivityTag";
    private ReceiverObservable res;
    private Disposable subscription;

    protected DataSource repository;
    protected static String token;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        token = getSharedPreferences(
                getPackageName(), Context.MODE_PRIVATE).getString("Token", "");

        repository = MainRepository.getInstance(RemoteDataSource.getInstance(token),
                LocalDataSource.getInstance(getSharedPreferences(getPackageName(), MODE_PRIVATE))
        );
        checkForUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();
        res = new ReceiverObservable(this, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        subscription = res.getObservable().subscribe(intent -> {

                    if (NetworkHelper.isConnected(this))
                        onConnectionEstablished();
                    else
                        onConnectionLost();
                },
                Throwable::printStackTrace);

        checkForCrashes();

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (subscription != null && !subscription.isDisposed())
            subscription.dispose();
        unregisterManagers();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterManagers();

    }

    public static String getToken() {
        return token;
    }

    protected void onConnectionEstablished() {
    }

    protected void onConnectionLost() {
    }


    private void checkForCrashes() {
//        CrashManager.register(this);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
//        UpdateManager.register(this);
    }

    private void unregisterManagers() {

//        UpdateManager.unregister();
    }

}
