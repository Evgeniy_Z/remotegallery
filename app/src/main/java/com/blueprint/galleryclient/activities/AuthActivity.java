package com.blueprint.galleryclient.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.blueprint.galleryclient.Const;
import com.blueprint.galleryclient.R;
import com.blueprint.galleryclient.presenters.AuthPresenter;
import com.blueprint.galleryclient.views.AuthView;

import retrofit2.HttpException;

public class AuthActivity extends AppBaseActivity implements AuthView {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auth_layout);
        AuthPresenter authPresenter = new AuthPresenter(this, repository);
        EditText loginEt, passwordEt;
        loginEt = findViewById(R.id.login_editText);
        passwordEt = findViewById(R.id.password_editText);
        findViewById(R.id.login_button).setOnClickListener(v -> {
            authPresenter.login(loginEt.getText().toString(), passwordEt.getText().toString());
        });
        findViewById(R.id.register_button).setOnClickListener(view -> {
            authPresenter.register(loginEt.getText().toString(), passwordEt.getText().toString());
        });
    }

    @Override
    public void onBackPressed() {
        setResult(Const.QUIT);
        finish();
        super.onBackPressed();
    }

    @Override
    public void showLoadingView() {

    }

    @Override
    public void hideLoadingView() {

    }

    @Override
    public void showAuthError() {
        Toast.makeText(this, "Some error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void authSuccess() {
        AuthActivity.this.setResult(RESULT_OK);
        AuthActivity.this.finish();
    }
}
