package com.blueprint.galleryclient.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.blueprint.galleryclient.R;
import com.blueprint.galleryclient.presenters.PictureOptionsPresenter;
import com.blueprint.galleryclient.data.repositories.FileSystemRepository;
import com.blueprint.galleryclient.views.PictureOptionsView;
import com.squareup.picasso.Picasso;

import java.io.File;

public class PictureOptionsActivity extends AppBaseActivity implements PictureOptionsView {

    private File imageFile;
    private ImageView pImageView;
    private final String _TAG = "PictureOptionsActivity";
    private PictureOptionsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_options);
        presenter = new PictureOptionsPresenter(this, repository,
                FileSystemRepository.getInstance(getBaseContext())
        );

        pImageView = findViewById(R.id.imagePreview);
        try {
            imageFile = (File) getIntent().getExtras().get("photo");
            Picasso.with(this).load(imageFile).into(pImageView);
            presenter.addImageFile(imageFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        findViewById(R.id.button_uploadImage).setOnClickListener(view -> {
            presenter.uploadImage();
        });
        findViewById(R.id.button_saveImage).setOnClickListener(view -> {
        });
        findViewById(R.id.button_deleteImage).setOnClickListener(view -> {
            presenter.cancel();
        });
        findViewById(R.id.progressBar).setVisibility(View.GONE);


    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onAttach(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onDettach();
    }

    @Override
    public void onBackPressed() {
        presenter.cancel();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void showLoadingView() {
        findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingView() {
        findViewById(R.id.progressBar).setVisibility(View.GONE);
    }

    @Override
    public void onUploadError() {
        Toast.makeText(this, "Failed to upload", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUploadFinished() {
        finish();
    }

    @Override
    public void onCanceled() {
        finish();
    }
}
