package com.blueprint.galleryclient.activities;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.blueprint.galleryclient.R;
import com.blueprint.galleryclient.presenters.PictureSliderPresenter;
import com.blueprint.galleryclient.views.PictureSliderView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;

import static android.support.v4.view.ViewPager.SCROLL_STATE_IDLE;

public class PictureSliderActivity extends AppBaseActivity implements PictureSliderView {

    private ViewPager viewPager;
    private MyAdapter adapter;
    private PictureSliderPresenter pictureSliderPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_slider);
        viewPager = findViewById(R.id.slider);
        adapter = new MyAdapter();
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == SCROLL_STATE_IDLE) {
                    pictureSliderPresenter.setPosition(viewPager.getCurrentItem());
                }

            }
        });
        pictureSliderPresenter = new PictureSliderPresenter(this, repository);
        pictureSliderPresenter.setPosition(getIntent().getIntExtra("position", 0));
        pictureSliderPresenter.loadImages();
    }

    @Override
    public void showLoadingView() {

    }

    @Override
    public void hideLoadingView() {

    }

    @Override
    public void showLoadedImages() {
        adapter.setPics(pictureSliderPresenter.getItems());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showSelectedImage() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void showImage(int imageNumber){
        viewPager.setCurrentItem(imageNumber, false);
    }


    class MyAdapter extends PagerAdapter{
        private List<String> pics = new ArrayList<>();

        public void setPics(List<String> pics) {
            this.pics = pics;
        }


        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            LayoutInflater inflater = LayoutInflater.from(getBaseContext());
            ViewGroup view = (ViewGroup) inflater.inflate(R.layout.img_page, container, false);
            Picasso.with(PictureSliderActivity.this)
                    .load(pics.get(position))
                    .placeholder(R.drawable.ic_launcher_background)
                    .into((ImageView) view.findViewById(R.id.imageView));
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            ((ViewPager) container).removeView((View) object);
        }

        @Override
        public int getCount() {
            return pics.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }
}
