package com.blueprint.galleryclient;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.blueprint.galleryclient.activities.PictureSliderActivity;
import com.blueprint.galleryclient.network.models.ImgResponse;
import com.blueprint.galleryclient.presenters.ImageListPresenter;
import com.blueprint.galleryclient.views.ImageItemView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created on 07.01.2018.
 */

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ImgViewHolder> {


    private static final String _TAG = "ImageItemAd_tag";
    private Context context;
    private ImageListPresenter imageListPresenter;
    private ItemClickListener clickListener;

    public ImageListAdapter(Context context, ImageListPresenter imageListPresenter) {
        this.context = context;
        this.imageListPresenter = imageListPresenter;
    }

    public void setList(ArrayList<ImgResponse> list) {
    }

    @Override
    public ImgViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(_TAG, "onCreateViewHolder: ");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.img_list_item, parent, false);
        ImgViewHolder viewHolder = new ImgViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ImgViewHolder holder, int position) {
        imageListPresenter.onBindViewToPosition(position, holder);
    }

    @Override
    public int getItemCount() {
        return imageListPresenter.getListCount();
    }

    public void setClickListener(ItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    class ImgViewHolder extends RecyclerView.ViewHolder implements ImageItemView {

        public TextView title;
        public ImageView image;

        public ImgViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);
            itemView.setOnClickListener(view -> {
                if(clickListener!=null){
                    clickListener.onItemClick(getAdapterPosition());
                }
//                context.startActivity(new Intent(context, PictureSliderActivity.class));
            });
        }

        @Override
        public void setImage(String url) {
            Log.d(_TAG, "setImage: "+url);
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.drawable.ic_launcher_background)
                    .centerCrop()
                    .fit()
                    .into(image);
        }

        @Override
        public void setTitle(String title) {
            Log.d(_TAG, "setTitle: "+title);
            this.title.setText(title);
        }
    }
}
