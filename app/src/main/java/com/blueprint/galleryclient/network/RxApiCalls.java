package com.blueprint.galleryclient.network;

import com.blueprint.galleryclient.network.models.ImgResponse;
import com.blueprint.galleryclient.network.models.LoginRequest;
import com.blueprint.galleryclient.network.models.LoginResponse;
import com.blueprint.galleryclient.network.models.RegisterRequest;
import com.blueprint.galleryclient.network.models.RegisterResponse;
import com.blueprint.galleryclient.network.models.UploadHeadPicRequest;
import com.blueprint.galleryclient.network.models.UserResponse;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created on 21.02.2018.
 */

public interface RxApiCalls {


    @POST("uusapp/register/")
    Observable<RegisterResponse> registerNewUser(@Body RegisterRequest request);

    @GET("uusapp/images/")
    Observable<List<ImgResponse>> getImageList();

    @GET("uusapp/checkauth/")
    Observable<String> isAuthenticated();

    @GET("uusapp/getuser/")
    Observable<UserResponse> getUserData();

    @POST("api-token-auth/")
    Observable<LoginResponse> login(@Body LoginRequest request);

    @Multipart
    @POST("uusapp/addimage/")
    Observable<ResponseBody> uploadImage(@Part MultipartBody.Part file,
                                   @Part ("title") String title);
    @Multipart
    @POST("uusapp/addimage/")
    Observable<ResponseBody> uploadImage(@Part MultipartBody.Part file,
                                   @Part MultipartBody.Part p);
    @Multipart
    @POST("uusapp/addimage/")
    Observable<ResponseBody> uploadImage(@Part MultipartBody.Part file,
                                   @Body UploadHeadPicRequest request);
}
