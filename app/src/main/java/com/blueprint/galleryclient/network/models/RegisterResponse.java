package com.blueprint.galleryclient.network.models;

import java.util.List;

/**
 * Created on 07.01.2018.
 */

public class RegisterResponse {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
