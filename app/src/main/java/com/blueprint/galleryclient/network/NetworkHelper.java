package com.blueprint.galleryclient.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.blueprint.galleryclient.data.RemoteDataSource;

import java.io.IOException;

/**
 * Created on 23.02.2018.
 */

public class NetworkHelper {

    public static boolean canConnectToServer() {
        String HOST_NAME = RemoteDataSource.HOST_NAME;
        String command = "ping -c 1 " + HOST_NAME;
        try {
            return (Runtime.getRuntime().exec(command).waitFor() == 0);
        } catch (InterruptedException e) {
            return false;
        } catch (IOException e) {
            return false;
        }

//
    }

    public static boolean isConnected(Context context){
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo connection = manager.getActiveNetworkInfo();
        return  (connection != null && connection.isConnected());
    }
}
