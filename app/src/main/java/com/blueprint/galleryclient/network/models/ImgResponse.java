package com.blueprint.galleryclient.network.models;

import com.blueprint.galleryclient.BuildConfig;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created on 07.01.2018.
 */

public class ImgResponse {
    private int id;
    private String title;
    private String image;
    @SerializedName("image_full")
    private String fullImage;
    private String owner;
    private Date created;
    private int group;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
//        if(image.startsWith(BuildConfig.SERVER))
//            image = BuildConfig.SERVER + image;
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public String getFullImage() {
        return fullImage;
    }

    public void setFullImage(String fullImage) {
        this.fullImage = fullImage;
    }
}
