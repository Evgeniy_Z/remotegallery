package com.blueprint.galleryclient.network.models;

/**
 * Created on 14.02.2018.
 */

public class UploadHeadPicRequest {
    private String title;

    public UploadHeadPicRequest(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
