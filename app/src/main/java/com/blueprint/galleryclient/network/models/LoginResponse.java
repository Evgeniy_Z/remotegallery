package com.blueprint.galleryclient.network.models;

/**
 * Created on 07.01.2018.
 */

public class LoginResponse {
    private String token;

    public String getToken() {
        return "Token "+token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
