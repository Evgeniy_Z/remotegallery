package com.blueprint.galleryclient.network;

import com.blueprint.galleryclient.network.models.ImgResponse;
import com.blueprint.galleryclient.network.models.LoginRequest;
import com.blueprint.galleryclient.network.models.LoginResponse;
import com.blueprint.galleryclient.network.models.RegisterRequest;
import com.blueprint.galleryclient.network.models.RegisterResponse;
import com.blueprint.galleryclient.network.models.UploadHeadPicRequest;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

/**
 * Created on 06.01.2018.
 */

public interface ApiCalls {

    @POST("register/")
    Call<RegisterResponse> registerNewUser(@Body RegisterRequest request);

    @GET("uusapp/images/")
    Call<List<ImgResponse>> getImageList();

    @GET("uusapp/checkauth/")
    Call<String> isAuthenticated();

    @POST("api-token-auth/")
    Call<LoginResponse> login(@Body LoginRequest request);

    @Multipart
    @POST("uusapp/addimage/")
    Call<ResponseBody> uploadImage(@Part MultipartBody.Part file,
                                   @Part ("title") String title);
    @Multipart
    @POST("uusapp/addimage/")
    Call<ResponseBody> uploadImage(@Part MultipartBody.Part file,
                                   @Part MultipartBody.Part p);
    @Multipart
    @POST("uusapp/addimage/")
    Call<ResponseBody> uploadImage(@Part MultipartBody.Part file,
                                   @Body UploadHeadPicRequest request);

}
