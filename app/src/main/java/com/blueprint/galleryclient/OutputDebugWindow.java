package com.blueprint.galleryclient;

import android.content.Context;

/**
 * Created on 24.02.2018.
 */

class OutputDebugWindow {
    private static OutputDebugWindow ourInstance;
    private Context context;

    static OutputDebugWindow getInstance(Context context) {
        if (ourInstance == null)
            ourInstance = new OutputDebugWindow(context);
        return ourInstance;
    }

    private OutputDebugWindow(Context context) {
        this.context = context;

    }
}
