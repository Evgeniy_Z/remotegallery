package com.blueprint.galleryclient;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;

/**
 * Created on 22.02.2018.
 */

public class ReceiverObservable {

    private static final String _TAG = "ReceiverObservable";
    private BroadcastReceiver broadcastReceiver;
    private Context context;
    private Flowable<Intent> observable;

    public ReceiverObservable(Context context, IntentFilter filter) {
        this.context = context;
        observable = create(filter);
    }

    private Flowable<Intent> create(IntentFilter filter) {
        Flowable<Intent> observable = Flowable.create((FlowableEmitter<Intent> e) -> {

            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Log.d(_TAG, "onReceive: received event");
                    e.onNext(intent);
                }
            };
            context.registerReceiver(broadcastReceiver, filter);
            e.setCancellable(() -> context.unregisterReceiver(broadcastReceiver));
        }, BackpressureStrategy.LATEST);
        return observable;
    }

    public Flowable<Intent> getObservable() {
        return observable;
    }


}
