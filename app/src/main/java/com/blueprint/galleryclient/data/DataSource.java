package com.blueprint.galleryclient.data;

import com.blueprint.galleryclient.network.models.ImgResponse;
import com.blueprint.galleryclient.network.models.LoginResponse;
import com.blueprint.galleryclient.network.models.UserResponse;

import java.io.File;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import okhttp3.ResponseBody;

/**
 * Created on 07.04.2018.
 */

public interface DataSource {
    Observable<Integer> register(String username, String password);

    Observable<LoginResponse> login(String login, String password);

    Observable<List<ImgResponse>> getImages();

    Observable<UserResponse> getUserData();

    Observable<Boolean> isAuthenticated();

    void setToken(String t);

    void onLoggedIn(String token);

    void logOut();

    Observable<ResponseBody> uploadImage(File file);

    void addLogOutListener(Observer<? super Void> listener);

    String getToken();
}
