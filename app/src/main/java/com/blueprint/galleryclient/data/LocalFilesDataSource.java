package com.blueprint.galleryclient.data;

import android.net.Uri;

import java.io.File;
import java.io.IOException;

/**
 * Created on 07.04.2018.
 */

public interface LocalFilesDataSource {
    File createImageFile() throws IOException;

    File getCurrentImageFile();

    File getImageFile(Uri uri);

    boolean deleteFile(File file);
}
