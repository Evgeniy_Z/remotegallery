package com.blueprint.galleryclient.data;

import android.content.SharedPreferences;

import com.blueprint.galleryclient.network.models.ImgResponse;
import com.blueprint.galleryclient.network.models.LoginResponse;
import com.blueprint.galleryclient.network.models.UserResponse;

import java.io.File;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import okhttp3.ResponseBody;

/**
 * Created on 28.02.2018.
 */

public class LocalDataSource implements DataSource {
    private static LocalDataSource instance;
    private SharedPreferences preferences;

    public static LocalDataSource getInstance(SharedPreferences preferences) {
        if (instance == null)
            instance = new LocalDataSource(preferences);
        return instance;
    }

    public LocalDataSource(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public Observable<Integer> register(String username, String password) {
        return null;
    }

    @Override
    public Observable<LoginResponse> login(String login, String password) {
        return null;
    }

    @Override
    public Observable<List<ImgResponse>> getImages() {
        return null;
    }

    @Override
    public Observable<UserResponse> getUserData() {
        return Observable.error(Throwable::new);
    }

    @Override
    public Observable<Boolean> isAuthenticated() {
        return Observable.just(true);
    }

    @Override
    public void setToken(String t) {

    }

    @Override
    public void onLoggedIn(String token) {
        saveToken(token);
    }

    @Override
    public void logOut() {

    }

    @Override
    public Observable<ResponseBody> uploadImage(File file) {
        return null;
    }

    @Override
    public void addLogOutListener(Observer<? super Void> listener) {

    }

    private void saveToken(String t) {
        preferences.edit().putString("Token", t).commit();
    }

    @Override
    public String getToken() {
        return preferences.getString("Token", "");
    }
}
