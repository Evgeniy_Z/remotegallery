package com.blueprint.galleryclient.data.repositories;

import com.blueprint.galleryclient.data.DataSource;
import com.blueprint.galleryclient.network.models.ImgResponse;
import com.blueprint.galleryclient.network.models.LoginResponse;
import com.blueprint.galleryclient.network.models.UserResponse;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import okhttp3.ResponseBody;

/**
 * Created on 13.04.2018.
 */

public class TestRepository implements DataSource {
    @Override
    public Observable<Integer> register(String username, String password) {
        return null;
    }

    @Override
    public Observable<LoginResponse> login(String login, String password) {
        return null;
    }

    @Override
    public Observable<List<ImgResponse>> getImages() {
        return null;
    }

    @Override
    public Observable<UserResponse> getUserData() {
        return null;
    }

    @Override
    public Observable<Boolean> isAuthenticated() {
        return null;
    }

    @Override
    public void setToken(String t) {

    }

    @Override
    public void onLoggedIn(String token) {

    }

    @Override
    public void logOut() {

    }

    @Override
    public Observable<ResponseBody> uploadImage(File file) {
        return null;
    }

    @Override
    public void addLogOutListener(Observer<? super Void> listener) {

    }

    @Override
    public String getToken() {
        return null;
    }
}
