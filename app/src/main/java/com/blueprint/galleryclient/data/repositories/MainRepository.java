package com.blueprint.galleryclient.data.repositories;

import com.blueprint.galleryclient.data.DataSource;
import com.blueprint.galleryclient.data.LocalDataSource;
import com.blueprint.galleryclient.data.RemoteDataSource;
import com.blueprint.galleryclient.network.models.ImgResponse;
import com.blueprint.galleryclient.network.models.LoginResponse;
import com.blueprint.galleryclient.network.models.UserResponse;

import java.io.File;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.subjects.PublishSubject;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

/**
 * Created on 27.02.2018.
 */

public class MainRepository implements DataSource {

    private RemoteDataSource remoteDataSource;
    private LocalDataSource localDataSource;
    private static DataSource instance;
    private final PublishSubject<Void> logOutSubject;

    public static DataSource getInstance(RemoteDataSource remoteDataSource,
                                         LocalDataSource localDataSource) {
        String t = localDataSource.getToken();
        remoteDataSource.setToken(t);
        if (instance == null)
            instance = new MainRepository(remoteDataSource, localDataSource);
        return instance;
    }

    private MainRepository(RemoteDataSource remoteDataSource, LocalDataSource localDataSource) {
        this.remoteDataSource = remoteDataSource;
        this.localDataSource = localDataSource;
        logOutSubject = PublishSubject.create();

    }

    @Override
    public Observable<Integer> register(String username, String password) {
        return remoteDataSource.register(username, password);

    }


    @Override
    public Observable<LoginResponse> login(String login, String password) {
        return remoteDataSource.login(login, password).retry(2);
    }


    @Override
    public Observable<List<ImgResponse>> getImages() {
        return remoteDataSource.getImages()
                .onErrorResumeNext((Throwable t) -> {
                    if (t instanceof HttpException && ((HttpException) t).code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                        return Observable.just(new ArrayList<ImgResponse>());
                    } else {
                        return Observable.error(t);
                    }
                });
    }

    @Override
    public Observable<UserResponse> getUserData() {
        return remoteDataSource.getUserData()
                .retry(5)
//                .onErrorResumeNext(localDataSource.getUserData())
                ;
    }

    @Override
    public Observable<Boolean> isAuthenticated() {
        return remoteDataSource.isAuthenticated()
                .retry(3)
                .onErrorResumeNext(localDataSource.isAuthenticated());
    }

    @Override
    public void setToken(String t) {

    }

    @Override
    public void onLoggedIn(String token) {
        remoteDataSource.onLoggedIn(token );
        localDataSource.onLoggedIn(token);
    }

    @Override
    public void logOut() {
        remoteDataSource.logOut();
        localDataSource.logOut();

    }

    @Override
    public Observable<ResponseBody> uploadImage(File file) {
        return remoteDataSource.uploadImage(file)
                .retry(3);
    }


    @Override
    public void addLogOutListener(Observer<? super Void> listener) {
        logOutSubject.subscribe(listener);
    }

    @Override
    public String getToken() {
        return null;
    }
}
