package com.blueprint.galleryclient.data;

import android.support.annotation.NonNull;
import android.util.Log;

import com.blueprint.galleryclient.BuildConfig;
import com.blueprint.galleryclient.network.RxApiCalls;
import com.blueprint.galleryclient.network.models.ImgResponse;
import com.blueprint.galleryclient.network.models.LoginRequest;
import com.blueprint.galleryclient.network.models.LoginResponse;
import com.blueprint.galleryclient.network.models.RegisterRequest;
import com.blueprint.galleryclient.network.models.UserResponse;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.HttpException;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created on 06.01.2018.
 */

public class RemoteDataSource implements DataSource {

    public static final String BASE_URL = BuildConfig.SERVER;

    public static final String HOST_NAME = BASE_URL.replace("http://", "")
            .replace("https://", "");
    private static final String _TAG = "ApiManagerTag";
    private static RemoteDataSource instance;
    private RxApiCalls client;
    private OkHttpClient httpClient;
    private static String token = "";


    private RemoteDataSource() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(new AuthInterceptor())
                .build();
        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .client(httpClient)
                        .baseUrl(BASE_URL)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        client = retrofit.create(RxApiCalls.class);

    }


    public static RemoteDataSource getInstance(String token) {
        RemoteDataSource.token = token;
        if (instance == null) {
            instance = new RemoteDataSource();
        }
        return instance;
    }

    private static Observable modifyObservable(Observable o) {
        return o.observeOn(Schedulers.io()).subscribeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Integer> register(String login, String password) {
        RegisterRequest request = new RegisterRequest();
        request.setUsername(login);
        request.setPassword(password);
        return client.registerNewUser(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(registerResponse -> {
                    return HttpURLConnection.HTTP_CREATED;
                })
                .onErrorResumeNext((Throwable t) -> {
                    if (t instanceof HttpException) {
                        return Observable.just(((HttpException) t).code());
                    }
                    return Observable.error(t);
                });
    }

    @Override
    public Observable<List<ImgResponse>> getImages() {
        return client.getImageList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<UserResponse> getUserData() {
        return client.getUserData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Boolean> isAuthenticated() {
        Observable ob = client.isAuthenticated()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Throwable t) -> {
                    if (t instanceof HttpException
                            && ((HttpException) t).code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                        return Observable.just("Fail");
                    }
                    return Observable.error(t);
                }).map(s -> {
                    Log.d(_TAG, "isAuthenticated: " + s);
                    if (s.equals("Fail"))
                        return false;
                    else
                        return true;
                });
        return ob;

    }

    public Observable<LoginResponse> login(String login, String password) {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(login);
        loginRequest.setPassword(password);
        return client.login(loginRequest).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> uploadImage(File file) {

        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("image/jpeg"),
                        file
                );
//        String data = new Gson().toJson(new UploadHeadPicRequest("Picture"));
//        RequestBody otherData = RequestBody.create(MediaType.parse("multipart/form-data"), data);
        // MultipartBody.Part is used to send also the actual file name
//        String descriptionString = "hello, this is description";
//        RequestBody description =
//                RequestBody.create(
//                        MultipartBody.FORM, descriptionString);
//        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//        MultipartBody.Part body = MultipartBody.Part.create(RequestBody.create(MediaType.))
        MultipartBody.Part part =
                MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        Observable<ResponseBody> call = client.uploadImage(part, file.getName());
        return call.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void addLogOutListener(Observer<? super Void> listener) {

    }

    @Override
    public String getToken() {
        return null;
    }

    @Override
    public void setToken(String t) {
        token = t;
    }

    @Override
    public void onLoggedIn(String token) {
        RemoteDataSource.token = token;
    }

    @Override
    public void logOut() {
        token = null;
    }

    public class AuthInterceptor implements Interceptor {


        @Override
        public Response intercept(@NonNull Chain chain) throws IOException {
            Request request = chain.request();
            if (token != null && !token.isEmpty()) {
                Request authenticatedRequest = request.newBuilder()
                        .header("Authorization", token).build();
                return chain.proceed(authenticatedRequest);
            } else {
                return chain.proceed(request);
            }
        }

    }
}
