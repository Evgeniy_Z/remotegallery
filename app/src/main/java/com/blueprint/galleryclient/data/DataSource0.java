package com.blueprint.galleryclient.data;

import com.blueprint.galleryclient.network.models.ImgResponse;
import com.blueprint.galleryclient.network.models.UserResponse;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created on 28.02.2018.
 */

public interface DataSource0 {
    Observable<List<ImgResponse>> getImages();

    Observable<UserResponse> getUserData();

    Observable<Boolean> isAuthenticated();

    void onLoggedIn(String token);

    void logOut();

}
