package com.blueprint.galleryclient.data.repositories;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.blueprint.galleryclient.data.LocalFilesDataSource;
import com.blueprint.galleryclient.presenters.MainActivityPresenter;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.zelory.compressor.Compressor;

import static com.blueprint.galleryclient.utils.FilePath.getPath;

/**
 * Created on 05.03.2018.
 */

public class FileSystemRepository implements LocalFilesDataSource {
    private static final String _TAG = "FileSystemRepositoryTag";
    private Context context;
    private MainActivityPresenter activityPresenter;
    private File currentImageFile;
    private static String temprImageFolderPath;

    private FileSystemRepository(Context context) {
        this.context = context;
        setDirectories();
    }

    public static LocalFilesDataSource getInstance(Context context){
        return new FileSystemRepository(context);
    }


    private void setDirectories() {
        temprImageFolderPath = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).getAbsolutePath();
        temprImageFolderPath += "/GalleryTempr";
        File d = new File(temprImageFolderPath);
        if (!d.exists()) {
            d.mkdir();
        }
    }


    @Override
    public File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
//        mCurrentPhotoPath = image.getAbsolutePath();
        currentImageFile = image;
        return image;
    }

    @Override
    public File getCurrentImageFile(){
        return currentImageFile;
    }

    @Override
    public File getImageFile(Uri uri){

        Log.d(_TAG, "getImageFile: uri "+uri);
        String p = getPath(context, uri);
        Log.d(_TAG, "getImageFile: path "+p);
        return new File(p);
    }

    private File resizeCurrentImage() {
        return resizeImage(currentImageFile);
    }

    private File resizeImage(Uri uri) {
        File sourceFile = new File(getPath(context, uri));
        return resizeImage(sourceFile);
    }

    private File resizeImage(File photoFile) {
        if (photoFile != null) {
            try {
                File compressedImage = new Compressor(context)
                        .setMaxWidth(640)
                        .setMaxHeight(480)
                        .setQuality(75)
                        .setCompressFormat(Bitmap.CompressFormat.WEBP)
                        .setDestinationDirectoryPath(temprImageFolderPath)
                        .compressToFile(photoFile);


                return compressedImage;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return photoFile;
    }

    @Override
    public boolean deleteFile(File file){
        return file.delete();
    }

//    public boolean deleteC

}
