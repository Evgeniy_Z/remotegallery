package com.blueprint.galleryclient;

/**
 * Created on 13.04.2018.
 */

public interface ItemClickListener {
    void onItemClick(int position);

    boolean onItemLongClick(int position);

}
