package com.blueprint.galleryclient.presenters;

import com.blueprint.galleryclient.network.models.ImgResponse;
import com.blueprint.galleryclient.data.DataSource;
import com.blueprint.galleryclient.views.GalleryView;
import com.blueprint.galleryclient.views.ImageItemView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 14.03.2018.
 */

public class ImageListPresenter extends Presenter<GalleryView> {

    private List<ImgResponse> list = new ArrayList<>();
    private DataSource repository;

    public ImageListPresenter(GalleryView view, DataSource repository) {
        this.repository = repository;
        onAttach(view);
    }

    public void onBindViewToPosition(int position, ImageItemView view) {
        view.setImage(list.get(position).getImage());
        view.setTitle(list.get(position).getTitle());

    }

    public void loadImages() {
        if(list.isEmpty()) getView().showLoadingView();
        repository.getImages().subscribe(imgResponses -> {
            list = imgResponses;
            getView().showLoadedImages();
            getView().hideLoadingView();
        }, throwable -> {
            getView().showError("");
            getView().hideLoadingView();
        });
    }


    public int getListCount() {
        return list.size();
    }

    @Override
    protected GalleryView getDummyView() {
        return new GalleryView() {
            @Override
            public void showLoadedImages() {

            }

            @Override
            public void showSelectedImage() {

            }

            @Override
            public void showError(String message) {

            }

            @Override
            public void showEmptyView() {

            }

            @Override
            public void showLoadingView() {

            }

            @Override
            public void hideLoadingView() {

            }
        };
    }

}
