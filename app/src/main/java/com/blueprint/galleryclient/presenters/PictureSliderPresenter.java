package com.blueprint.galleryclient.presenters;

import com.blueprint.galleryclient.data.DataSource;
import com.blueprint.galleryclient.network.models.ImgResponse;
import com.blueprint.galleryclient.views.GalleryView;
import com.blueprint.galleryclient.views.PictureSliderView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 13.04.2018.
 */

public class PictureSliderPresenter extends Presenter<PictureSliderView> {

    private final DataSource repository;
    private List<String> items = new ArrayList<>();
    private static int position;


    public PictureSliderPresenter(PictureSliderView view, DataSource repository) {
        this.repository = repository;
        onAttach(view);
    }

    public void loadImages() {
        repository.getImages().doOnSubscribe(disposable -> {
            getView().showLoadingView();
        })
                .doFinally(() -> {
                    getView().hideLoadingView();
                })
                .subscribe(imgResponses -> {
                    items.clear();
                    for (ImgResponse imgRespons : imgResponses) {
                        items.add(imgRespons.getFullImage());
                    }
                    getView().showLoadedImages();
                    getView().showImage(position);
                }, throwable -> {
                    throwable.printStackTrace();
                    getView().showError(throwable.getMessage());
                });
    }

    public List<String> getItems() {
        return items;
    }

    @Override
    protected PictureSliderView getDummyView() {
        return new PictureSliderView() {


            @Override
            public void showImage(int imageNumber) {

            }

            @Override
            public void showLoadedImages() {

            }

            @Override
            public void showSelectedImage() {

            }

            @Override
            public void showError(String message) {

            }

            @Override
            public void showEmptyView() {

            }

            @Override
            public void showLoadingView() {

            }

            @Override
            public void hideLoadingView() {

            }
        };
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
