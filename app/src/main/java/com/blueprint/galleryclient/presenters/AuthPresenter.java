package com.blueprint.galleryclient.presenters;

import android.util.Log;

import com.blueprint.galleryclient.data.DataSource;
import com.blueprint.galleryclient.views.AuthView;

/**
 * Created on 26.02.2018.
 */

public class AuthPresenter extends Presenter<AuthView> {
    private static final String _TAG = "AuthPresenterTag";
    DataSource repository;

    public AuthPresenter(AuthView authView, DataSource repository) {
        this.repository = repository;
        onAttach(authView);
    }

    public void register(String userName, String password) {
        repository.register(userName, password).subscribe(code -> {
            Log.d(_TAG, "register: code "+code);
            if (code >= 400) {
                getView().showAuthError();
            } else {
//                getView().authSuccess();
                login(userName, password);
            }
        }, throwable -> {
            getView().showAuthError();
            throwable.printStackTrace();
        });
    }

    public void login(String userName, String password) {
        repository.login(userName, password).subscribe(loginResponse -> {
            repository.onLoggedIn(loginResponse.getToken());
            getView().authSuccess();
        }, throwable -> {
            getView().showAuthError();
            throwable.printStackTrace();
        });
    }

    @Override
    protected AuthView getDummyView() {
        return new AuthView() {
            @Override
            public void showAuthError() {

            }

            @Override
            public void authSuccess() {

            }

            @Override
            public void showLoadingView() {

            }

            @Override
            public void hideLoadingView() {

            }
        };
    }
}
