package com.blueprint.galleryclient.presenters;

import com.blueprint.galleryclient.data.DataSource;
import com.blueprint.galleryclient.views.GalleryView;

/**
 * Created on 05.03.2018.
 */

public class GalleryPresenter extends Presenter<GalleryView> {
    private DataSource repository;

    public GalleryPresenter(GalleryView view, DataSource repository) {
        this.repository = repository;
        onAttach(view);
    }

    public void loadImages(){
        repository.getImages()
                .subscribe(imgResponses -> {
                    getView().showLoadedImages();
                }, throwable -> {
                    getView().showError("");
                });
    }


    @Override
    protected GalleryView getDummyView() {
        return new GalleryView() {


            @Override
            public void showLoadedImages() {

            }

            @Override
            public void showSelectedImage() {

            }

            @Override
            public void showError(String message) {

            }

            @Override
            public void showEmptyView() {

            }

            @Override
            public void showLoadingView() {

            }

            @Override
            public void hideLoadingView() {

            }
        };
    }
}
