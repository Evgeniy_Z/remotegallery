package com.blueprint.galleryclient.presenters;

import android.util.Log;

import com.blueprint.galleryclient.network.models.UserResponse;
import com.blueprint.galleryclient.data.DataSource;
import com.blueprint.galleryclient.views.UserInfoView;

/**
 * Created on 01.03.2018.
 */

public class UserInfoPresenter extends Presenter<UserInfoView> {

    private static final String _TAG = "UserInfoPresenterTag";
    private static UserResponse userResponse;
    private DataSource repository;

    public UserInfoPresenter(UserInfoView view, DataSource repository) {
        this.repository = repository;
        onAttach(view);
    }

    public void loadUserInfo() {
        if(userResponse!=null){
            getView().setUserName(userResponse.getUsername());
            getView().setUserAvatar(userResponse.getAvatar());
        }else {
            load();
        }
    }
    public void forceLoadUserData(){
        load();
    }

    private void load() {
        repository.getUserData().subscribe(userResponse -> {
            Log.d(_TAG, "loadUserInfo: name = " + userResponse.getUsername());
            UserInfoPresenter.userResponse = userResponse;
            getView().setUserName(userResponse.getUsername());
            getView().setUserAvatar(userResponse.getAvatar());
        }, Throwable::printStackTrace);
    }

    @Override
    protected UserInfoView getDummyView() {
        return new UserInfoView() {
            @Override
            public void setUserName(String name) {

            }

            @Override
            public void setUserAvatar(String url) {

            }
        };
    }
}
