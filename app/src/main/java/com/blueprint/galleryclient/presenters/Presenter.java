package com.blueprint.galleryclient.presenters;

/**
 * Created on 14.03.2018.
 */

public abstract class Presenter<T> {
    private T view;

    public void onAttach(T view) {
        this.view = view;
    }

    public void onDettach() {
        view = null;
    }

    protected T getView() {
        if (view == null) return getDummyView();
        return view;
    }


    protected abstract T getDummyView();
}
