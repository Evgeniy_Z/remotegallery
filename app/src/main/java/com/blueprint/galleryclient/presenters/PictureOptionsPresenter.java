package com.blueprint.galleryclient.presenters;

import com.blueprint.galleryclient.data.LocalFilesDataSource;
import com.blueprint.galleryclient.data.DataSource;
import com.blueprint.galleryclient.views.PictureOptionsView;

import java.io.File;

/**
 * Created on 16.03.2018.
 */

public class PictureOptionsPresenter extends Presenter<PictureOptionsView> {
    private DataSource repository;
    private LocalFilesDataSource localFilesRepository;
    private File image;

    public PictureOptionsPresenter(PictureOptionsView view, DataSource repository, LocalFilesDataSource localFilesRepository) {
        this.repository = repository;
        this.localFilesRepository = localFilesRepository;
        onAttach(view);
    }

    public void  addImageFile(File file){
        image = file;
    }

    public void uploadImage() {
        if(image == null){
            getView().onUploadError();
            return;
        }
        getView().showLoadingView();
        repository.uploadImage(image)
                .doFinally(() -> {
                    getView().hideLoadingView();
                })
                .subscribe(responseBody -> {
//                    fileSystemRepository.deleteFile(image);
                    getView().onUploadFinished();
                },
                throwable -> {
                    getView().onUploadError();
                });
    }

    public void cancel(){
//        fileSystemRepository.deleteFile(image);
        getView().onCanceled();
    }

    @Override
    protected PictureOptionsView getDummyView() {
        return new PictureOptionsView() {
            @Override
            public void onUploadError() {

            }

            @Override
            public void onUploadFinished() {

            }

            @Override
            public void onCanceled() {

            }

            @Override
            public void showLoadingView() {

            }

            @Override
            public void hideLoadingView() {

            }
        };
    }
}
