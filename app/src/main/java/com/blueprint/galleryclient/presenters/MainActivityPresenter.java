package com.blueprint.galleryclient.presenters;

import android.net.Uri;
import android.util.Log;

import com.blueprint.galleryclient.data.LocalFilesDataSource;
import com.blueprint.galleryclient.data.DataSource;
import com.blueprint.galleryclient.views.MainActivityView;

import java.io.File;
import java.io.IOException;

/**
 * Created on 05.03.2018.
 */

public class MainActivityPresenter extends Presenter<MainActivityView> {

    private static final String _TAG = "MainActivityPresenter";
    private LocalFilesDataSource localFilesRepository;
    private DataSource repository;

    public MainActivityPresenter(LocalFilesDataSource localFilesRepository,
                                 DataSource repository,
                                 MainActivityView view) {
        this.localFilesRepository = localFilesRepository;
        this.repository = repository;
        onAttach(view);
    }

    public void takeImageFromCamera() {

        File photoFile = null;
        try {
            photoFile = localFilesRepository.createImageFile();
        } catch (IOException ex) {
            // Error occurred while creating the File
        }
        if (photoFile != null) {
            getView().startCamera(photoFile);
        }
    }

    public File getImageTakenFromCamera() {
        return localFilesRepository.getCurrentImageFile();
    }

    public File getImageTakenFromGallery(Uri uri) {
        return localFilesRepository.getImageFile(uri);
    }

    public void checkUserAuthorization() {
        repository.isAuthenticated().subscribe(b -> {
                    Log.d(_TAG, "isAuthenticated_bool "+b);
                    if (!b)
                        getView().showLoginScreen();
                },
                throwable -> {

                });
    }


    @Override
    protected MainActivityView getDummyView() {
        return new MainActivityView() {
            @Override
            public void selectNavItem(int n) {

            }

            @Override
            public void startCamera(File photoFile) {

            }

            @Override
            public void showLoginScreen() {

            }

            @Override
            public void showLoadingView() {

            }

            @Override
            public void hideLoadingView() {

            }
        };
    }
}
