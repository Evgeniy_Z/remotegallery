package com.blueprint.galleryclient.views;

/**
 * Created on 26.02.2018.
 */

public interface BaseView {

    void showLoadingView();

    void hideLoadingView();
}
