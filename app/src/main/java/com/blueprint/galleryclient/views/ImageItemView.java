package com.blueprint.galleryclient.views;

/**
 * Created on 14.03.2018.
 */

public interface ImageItemView {

    void setImage(String url);

    void setTitle(String title);
}
