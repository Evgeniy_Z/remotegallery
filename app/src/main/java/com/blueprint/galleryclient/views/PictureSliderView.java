package com.blueprint.galleryclient.views;

/**
 * Created on 13.04.2018.
 */

public interface PictureSliderView extends GalleryView {
    void showImage(int imageNumber);
}
