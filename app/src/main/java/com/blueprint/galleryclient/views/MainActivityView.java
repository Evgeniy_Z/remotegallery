package com.blueprint.galleryclient.views;

import java.io.File;

/**
 * Created on 05.03.2018.
 */

public interface MainActivityView extends BaseView {

    void selectNavItem(int n);

    void startCamera(File photoFile);

    void showLoginScreen();

}
