package com.blueprint.galleryclient.views;

import com.blueprint.galleryclient.network.models.ImgResponse;

import java.util.List;

/**
 * Created on 05.03.2018.
 */

public interface GalleryView extends BaseView {

    void showLoadedImages();

    void showSelectedImage();

    void showError(String message);

    void showEmptyView();

}
