package com.blueprint.galleryclient.views;

/**
 * Created on 01.03.2018.
 */

public interface UserInfoView{

    void setUserName(String name);

    void setUserAvatar(String url);

}
