package com.blueprint.galleryclient.views;

/**
 * Created on 26.02.2018.
 */

public interface AuthView  extends BaseView{

    void showAuthError();

    void authSuccess();

}
