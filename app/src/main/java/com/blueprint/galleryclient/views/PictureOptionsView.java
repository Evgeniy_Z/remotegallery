package com.blueprint.galleryclient.views;

/**
 * Created on 16.03.2018.
 */

public interface PictureOptionsView extends BaseView{
    void onUploadError();

    void onUploadFinished();

    void onCanceled();
}
