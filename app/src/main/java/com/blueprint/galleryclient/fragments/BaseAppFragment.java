package com.blueprint.galleryclient.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.blueprint.galleryclient.data.LocalDataSource;
import com.blueprint.galleryclient.data.RemoteDataSource;
import com.blueprint.galleryclient.data.repositories.MainRepository;
import com.blueprint.galleryclient.data.DataSource;

/**
 * Created on 05.03.2018.
 */

public abstract class BaseAppFragment extends Fragment {
    protected DataSource repository;
    protected String token;


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String action);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        SharedPreferences pref = getContext().getSharedPreferences(getContext().getPackageName(), Context.MODE_PRIVATE);
        repository = MainRepository.getInstance(RemoteDataSource.getInstance(token),
                LocalDataSource.getInstance(pref)
        );
    }

}
