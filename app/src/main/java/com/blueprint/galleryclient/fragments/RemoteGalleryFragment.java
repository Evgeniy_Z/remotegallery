package com.blueprint.galleryclient.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.blueprint.galleryclient.GridAutofitLayoutManager;
import com.blueprint.galleryclient.ImageListAdapter;
import com.blueprint.galleryclient.ItemClickListener;
import com.blueprint.galleryclient.R;
import com.blueprint.galleryclient.activities.PictureSliderActivity;
import com.blueprint.galleryclient.presenters.ImageListPresenter;
import com.blueprint.galleryclient.views.GalleryView;

public class RemoteGalleryFragment extends BaseAppFragment implements GalleryView, ItemClickListener {

    private static final String _TAG = "GalleryFragmentTag";
    private OnFragmentInteractionListener mListener;
    private SwipeRefreshLayout refreshLayout;
    private ImageListAdapter imageListAdapter;
    private ImageListPresenter imageListPresenter;

    public RemoteGalleryFragment() {
        // Required empty public constructor
    }

    public static RemoteGalleryFragment newInstance() {
        RemoteGalleryFragment fragment = new RemoteGalleryFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        imageListPresenter.loadImages();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_remote_gallery, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.image_rv);
        recyclerView.setLayoutManager(
                new GridAutofitLayoutManager(getActivity(), (int) getContext().getResources().getDimension(R.dimen.cell_size))
        );
        imageListPresenter = new ImageListPresenter(this, repository);
        imageListAdapter = new ImageListAdapter(getContext(), imageListPresenter);
        imageListAdapter.setClickListener(this);
        recyclerView.setAdapter(imageListAdapter);

        refreshLayout = view.findViewById(R.id.refresh_layout);
        refreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark);
        refreshLayout.setProgressViewOffset(false,
                getResources().getDimensionPixelSize(R.dimen.img_list_offset),
                getResources().getDimensionPixelSize(R.dimen.refresher_offset_end));
        refreshLayout.setOnRefreshListener(() -> {
            imageListPresenter.loadImages();
        });


        return view;
    }


    public void onEvent(String uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            mListener = null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void showLoadingView() {
        refreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoadingView() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void showLoadedImages() {
        imageListAdapter.notifyDataSetChanged();
//        refreshLayout.setRefreshing(false);
    }

    @Override
    public void showSelectedImage() {

    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), "Connection error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(getActivity(), PictureSliderActivity.class);
        intent.putExtra("position", position);
        getActivity().startActivity(intent);
    }

    @Override
    public boolean onItemLongClick(int position) {
        return false;
    }
}
