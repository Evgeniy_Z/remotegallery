package com.blueprint.galleryclient.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.blueprint.galleryclient.R;
import com.blueprint.galleryclient.presenters.UserInfoPresenter;
import com.blueprint.galleryclient.utils.RoundedTransformation;
import com.blueprint.galleryclient.views.UserInfoView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

/**
 * Created on 09.03.2018.
 */

public class AccountFragment extends BaseAppFragment implements UserInfoView{

    private UserInfoPresenter userInfoPresenter;
    private TextView userNameTV;
    private ImageView userAvatarIV;

    public AccountFragment() {
        // Required empty public constructor
    }

    public static AccountFragment newInstance() {
        AccountFragment fragment = new AccountFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userInfoPresenter = new UserInfoPresenter(this, repository);
    }

    @Override
    public void onResume() {
        super.onResume();
        userInfoPresenter.loadUserInfo();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v;
        v = inflater.inflate(R.layout.account_fragment, container, false);
        initViewVariables(v);
        userInfoPresenter.loadUserInfo();
        return v;
    }

    private void initViewVariables(View v) {
        userNameTV = v.findViewById(R.id.user_name);
        userAvatarIV = v.findViewById(R.id.user_avatar);
    }

    @Override
    public void setUserName(String name) {
        userNameTV.setText(name);
    }

    @Override
    public void setUserAvatar(String url) {
        Picasso.with(getActivity())
                .load(url)
                .centerCrop()
                .fit()
                .into(userAvatarIV);
    }
}
