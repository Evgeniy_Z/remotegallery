package com.blueprint.galleryclient;

/**
 * Created on 13.01.2018.
 */

public class Const {
    public static final int REQUEST_TAKE_PHOTO = 110;
    public static final int PICK_IMAGE = 111;
    public static final int AUTH_REQUEST = 112;

    public static final int QUIT = 211;
}
