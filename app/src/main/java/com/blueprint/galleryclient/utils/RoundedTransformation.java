package com.blueprint.galleryclient.utils;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
 
// enables hardware accelerated rounded corners
// original idea here : http://www.curious-creature.org/2012/12/11/android-recipe-1-image-with-rounded-corners/
public class RoundedTransformation implements com.squareup.picasso.Transformation {
    private int radius;
    private int margin;  // dp
    private int color;
    private String key = "";

    // radius is corner radii in dp
    // margin is the board in dp


    public RoundedTransformation(int radius) {
        this.radius = radius;
    }

    public RoundedTransformation(int radius, int margin, String key) {
        this.radius = radius;
        this.margin = margin;
        this.key = key;
    }

    public RoundedTransformation(int radius, int margin, int color, String key) {
        this.radius = radius;
        this.margin = margin;
        this.color = color;
        this.key = key;
    }

    public RoundedTransformation( int radius,  int margin) {
        this.radius = radius;
        this.margin = margin;
    }
  public RoundedTransformation( int radius,  int margin, int borderColor) {
        this.radius = radius;
        this.margin = margin;
        this.color = borderColor;
    }

    @Override
    public Bitmap transform(final Bitmap source) {
        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setShader(new BitmapShader(source, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));

        Paint col = new Paint();
        col.setAntiAlias(true);
        col.setColor(color);
 
        Bitmap output = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        canvas.drawRoundRect(new RectF(0, 0, source.getWidth(), source.getHeight()), radius, radius, col );
        canvas.drawRoundRect(new RectF(margin, margin, source.getWidth() - margin, source.getHeight() - margin), radius, radius, paint);
 
        if (source != output) {
            source.recycle();
        }
 
        return output;
    }
 
    @Override
    public String key() {
        return "rounded"+key;
    }
}